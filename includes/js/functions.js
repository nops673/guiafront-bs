


$(document).ready(
    function () {
        tooltipsterStart()
    }
);




function liveSearch() {
    var inputValue = $('#liveSearchInput').val();
    if (inputValue != '') {
        $('section').each(function (section) {
            var arrayThisTitle = $(this).find('h1').text().split();

            if (($(this).find('h1').text().toLowerCase().indexOf(inputValue.toLowerCase())) < 0) {
                $(this).fadeOut();
            } else {
                $(this).fadeIn();
            }
        })
    } else {
        $('section').each(function (section) {
            $(this).fadeIn();
        })
    }
}

function tooltipsterStart() {
    $('.clickToCopy')
        .tooltipster({
            theme: 'tooltipster-shadow',
            content: 'Copiar este componente',
            side: 'top'
        })
        .tooltipster({
            theme: 'tooltipster-shadow',
            // contentAsHTML: true,
            content: 'Copiado !',
            // contentCloning: true,
            // interactive: true,
            trigger: 'custom',
            multiple: true,
            triggerOpen: {
                click: true
            },
            triggerClose: {
                mouseleave: true
            },

            functionBefore: function (instance, helper) {
                clickToCopy(instance.elementOrigin());
            }
        })

}

function clickToCopy(elemento) {
    var elementoCopiado = $(elemento);
    elementoCopiado.removeClass('clickToCopy');
    elementoCopiado.removeClass('tooltipstered');
    var elementoCopiadoString = elementoCopiado[0].outerHTML;
    elementoCopiado.addClass('clickToCopy');
    elementoCopiado.addClass('tooltipstered');


    $('#inputToCopy').val(elementoCopiadoString);
    $('#inputToCopy')[0].select();
    $('#inputToCopy')[0].setSelectionRange(0, 99999);

    document.execCommand("copy");

    // console.log('Texto copiado: ' + $('#inputToCopy').val());
    // $('#inputToCopy').val('');
}